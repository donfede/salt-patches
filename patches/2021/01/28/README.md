Patches for the following CVEs
==============================

CVE-2020-28243 CVE-2020-28972 CVE-2020-35662 CVE-2021-3148 CVE-2021-3144
CVE-2021-25281 CVE-2021-25282 CVE-2021-25283 CVE-2021-25284
CVE-2021-3197


[3002.2](3002.2.patch)


[3001.4](3001.4.patch)


[3000.6](3000.6.patch)


[2019.2.8](2019.2.8.patch)


[2019.2.5](2019.2.5.patch)


[2018.3.5](2018.3.5.patch)


[2017.7.8](2017.7.8.patch)


[2016.11.10](2016.11.10.patch)


[2016.11.6](2016.11.6.patch)


[2016.11.5](2016.11.5.patch)


[2016.11.3](2016.11.3.patch)


[2016.3.8](2016.3.8.patch)


[2016.3.6](2016.3.6.patch)


[2016.3.4](2016.3.4.patch)


[2015.8.13](2015.8.13.patch)


[2015.8.10](2015.8.10.patch)
