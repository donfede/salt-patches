From e161eb876f7df15a0af6d68a3f7e87491a11ddd6 Mon Sep 17 00:00:00 2001
From: "Daniel A. Wozniak" <dwozniak@vmware.com>
Date: Wed, 3 Feb 2021 16:00:05 -0700
Subject: [PATCH] Runners fix

---
 salt/client/mixins.py  | 37 +++++++++++++---
 salt/master.py         |  3 +-
 salt/runner.py         |  3 +-
 salt/utils/vmware.py   | 99 +++++++++++++++++++++++++-----------------
 salt/wheel/__init__.py |  2 +-
 5 files changed, 94 insertions(+), 50 deletions(-)

diff --git a/salt/client/mixins.py b/salt/client/mixins.py
index 8d3097e8a7..eb0c57b1ad 100644
--- a/salt/client/mixins.py
+++ b/salt/client/mixins.py
@@ -415,10 +415,10 @@ class AsyncClientMixin(object):
     client = None
     tag_prefix = None
 
-    def _proc_function(self, fun, low, user, tag, jid, daemonize=True):
+    def _proc_function_remote(self, fun, low, user, tag, jid, daemonize=True):
         '''
-        Run this method in a multiprocess target to execute the function in a
-        multiprocess and fire the return data on the event bus
+        Run this method in a multiprocess target to execute the function on the
+        master and fire the return data on the event bus
         '''
         if daemonize:
             # Shutdown the multiprocessing before daemonizing
@@ -439,6 +439,27 @@ class AsyncClientMixin(object):
         except salt.exceptions.EauthAuthenticationError as exc:
             log.error(exc)
 
+    def _proc_function(self, fun, low, user, tag, jid, daemonize=True):
+        '''
+        Run this method in a multiprocess target to execute the function
+        locally and fire the return data on the event bus
+        '''
+        if daemonize and not salt.utils.platform.is_windows():
+            # Shutdown the multiprocessing before daemonizing
+            salt.log.setup.shutdown_multiprocessing_logging()
+
+            salt.utils.process.daemonize()
+
+            # Reconfigure multiprocessing logging after daemonizing
+            salt.log.setup.setup_multiprocessing_logging()
+
+        # pack a few things into low
+        low["__jid__"] = jid
+        low["__user__"] = user
+        low["__tag__"] = tag
+
+        return self.low(fun, low)
+
     def cmd_async(self, low):
         '''
         Execute a function asynchronously; eauth is respected
@@ -465,15 +486,19 @@ class AsyncClientMixin(object):
         tag = tagify(jid, prefix=self.tag_prefix)
         return {'tag': tag, 'jid': jid}
 
-    def async(self, fun, low, user='UNKNOWN'):
+    def async(self, fun, low, user='UNKNOWN', local=True):
         '''
         Execute the function in a multiprocess and return the event tag to use
         to watch for the return
         '''
+        if local:
+            proc_func = self._proc_function
+        else:
+            proc_func = self._proc_function_remote
         async_pub = self._gen_async_pub()
-
         proc = SignalHandlingMultiprocessingProcess(
-                target=self._proc_function,
+                target=proc_func,
+                name="ProcessFunc",
                 args=(fun, low, user, async_pub['tag'], async_pub['jid']))
         with default_signals(signal.SIGINT, signal.SIGTERM):
             # Reset current signals before starting the process in
diff --git a/salt/master.py b/salt/master.py
index 091dfd7a21..e7a7e352cb 100644
--- a/salt/master.py
+++ b/salt/master.py
@@ -1766,7 +1766,8 @@ class ClearFuncs(object):
                 runner_client = salt.runner.RunnerClient(self.opts)
                 return runner_client.async(fun,
                                            clear_load.get('kwarg', {}),
-                                           clear_load.pop('username', 'UNKNOWN'))
+                                           clear_load.pop('username', 'UNKNOWN'),
+                                           local=True)
             except Exception as exc:
                 log.error('Exception occurred while '
                           'introspecting {0}: {1}'.format(fun, exc))
diff --git a/salt/runner.py b/salt/runner.py
index 8054879960..73ff18d8ec 100644
--- a/salt/runner.py
+++ b/salt/runner.py
@@ -188,7 +188,8 @@ class Runner(RunnerClient):
                                           user,
                                           async_pub['tag'],
                                           async_pub['jid'],
-                                          False)  # Don't daemonize
+                                          daemonize=False)  # Don't daemonize
+
             except salt.exceptions.SaltException as exc:
                 ret = '{0}'.format(exc)
                 if not self.opts.get('quiet', False):
diff --git a/salt/utils/vmware.py b/salt/utils/vmware.py
index 39b5c8ab90..6f0f918ca4 100644
--- a/salt/utils/vmware.py
+++ b/salt/utils/vmware.py
@@ -78,6 +78,7 @@ import atexit
 import errno
 import logging
 import time
+import ssl
 from salt.ext.six.moves.http_client import BadStatusLine  # pylint: disable=E0611
 
 # Import Salt Libs
@@ -188,58 +189,74 @@ def get_service_instance(host, username, password, protocol=None, port=None, ver
     if port is None:
         port = 443
 
+    default_msg = (
+        "Could not connect to host '{0}'. "
+        "Please check the debug log for more information.".format(host)
+    )
+
     service_instance = GetSi()
     if service_instance:
         if service_instance._GetStub().host == ':'.join([host, str(port)]):
             return service_instance
         Disconnect(service_instance)
-
-    try:
-        service_instance = SmartConnect(
-            host=host,
-            user=username,
-            pwd=password,
-            protocol=protocol,
-            port=port
-            verify_ssl=verify_ssl
-        )
-    except Exception as exc:
-        default_msg = 'Could not connect to host \'{0}\'. ' \
-                      'Please check the debug log for more information.'.format(host)
+    if verify_ssl:
         try:
-            if (isinstance(exc, vim.fault.HostConnectFault) and '[SSL: CERTIFICATE_VERIFY_FAILED]' in exc.msg) or '[SSL: CERTIFICATE_VERIFY_FAILED]' in str(exc):
-                import ssl
-                default_context = ssl._create_default_https_context
-                ssl._create_default_https_context = ssl._create_unverified_context
-                service_instance = SmartConnect(
-                    host=host,
-                    user=username,
-                    pwd=password,
-                    protocol=protocol,
-                    port=port
+            service_instance = SmartConnect(
+                host=host,
+                user=username,
+                pwd=password,
+                protocol=protocol,
+                port=port
+            )
+        except Exception as exc:  # pylint: disable=broad-except
+            # pyVmomi's SmartConnect() actually raises Exception in some cases.
+            if (
+                isinstance(exc, vim.fault.HostConnectFault)
+                and "[SSL: CERTIFICATE_VERIFY_FAILED]" in exc.msg
+            ) or "[SSL: CERTIFICATE_VERIFY_FAILED]" in str(exc):
+                err_msg = (
+                    "Could not verify the SSL certificate. You can use "
+                    "verify_ssl: False if you do not want to verify the "
+                    "SSL certificate. This is not recommended as it is "
+                    "considered insecure."
                 )
-                ssl._create_default_https_context = default_context
             else:
-                err_msg = exc.msg if hasattr(exc, 'msg') else default_msg
-                log.debug(exc)
-                raise SaltSystemExit(err_msg)
-
-        except Exception as exc:
-            if 'certificate verify failed' in str(exc):
-                import ssl
+                log.exception(exc)
+                err_msg = exc.msg if hasattr(exc, "msg") else default_msg
+            raise SaltSystemExit(err_msg)
+    else:
+        try:
+            service_instance = SmartConnect(
+                host=host,
+                user=username,
+                pwd=password,
+                protocol=protocol,
+                port=port,
+                sslContext=getattr(ssl, '_create_unverified_context', getattr(ssl, '_create_stdlib_context'))()
+            )
+        except Exception as exc:  # pylint: disable=broad-except
+            # pyVmomi's SmartConnect() actually raises Exception in some cases.
+            if "certificate verify failed" in str(exc):
                 context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
                 context.verify_mode = ssl.CERT_NONE
-                service_instance = SmartConnect(
-                    host=host,
-                    user=username,
-                    pwd=password,
-                    protocol=protocol,
-                    port=port,
-                    sslContext=context
-                )
+                try:
+                    service_instance = SmartConnect(
+                        host=host,
+                        user=username,
+                        pwd=password,
+                        protocol=protocol,
+                        port=port,
+                        sslContext=context
+                    )
+                except Exception as exc:  # pylint: disable=broad-except
+                    log.exception(exc)
+                    err_msg = exc.msg if hasattr(exc, "msg") else str(exc)
+                    raise SaltSystemExit(
+                        "Could not connect to host '{}': " "{}".format(host, err_msg)
+                    )
             else:
-                err_msg = exc.msg if hasattr(exc, 'msg') else default_msg
-                log.debug(exc)
+                err_msg = exc.msg if hasattr(exc, "msg") else default_msg
+                log.trace(exc)
                 raise SaltSystemExit(err_msg)
 
     atexit.register(Disconnect, service_instance)
diff --git a/salt/wheel/__init__.py b/salt/wheel/__init__.py
index 2c342b1c59..e234c8e411 100644
--- a/salt/wheel/__init__.py
+++ b/salt/wheel/__init__.py
@@ -110,7 +110,7 @@ class WheelClient(mixins.SyncClientMixin, mixins.AsyncClientMixin, object):
             {'jid': '20131219224744416681', 'tag': 'salt/wheel/20131219224744416681'}
         '''
         fun = low.get('fun')
-        return self.async(fun, low)
+        return self.async(fun, low, local=False)
 
     def cmd(self, fun, arg=None, pub_data=None, kwarg=None, print_event=True):
         '''
-- 
2.29.2

